LockNow
=======

A status item that locks the screen when clicked, because I was tired of accidentally 
locking the keychain instead of the screen when using the Keychain Access status item

To quit, ⌘-click the status item

Code is provided as-is. Safety not guaranteed